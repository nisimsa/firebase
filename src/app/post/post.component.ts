import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  post:Post;
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  originalTitle = '';
  originalBody = '';

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit (this.post);
  }

  sendUpdate(){
    this.editEvent.emit (this.post);
  }

  toggleEdit(){
    this.originalTitle = this.post.title;
    this.originalBody = this.post.body;

    if (this.isEdit){
      this.sendUpdate()
    }

    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }

  cancelEdit() {
    this.post.title = this.originalTitle;
    this.post.body = this.originalBody;
    this.isEdit = false;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }

  ngOnInit() {
  }

}
