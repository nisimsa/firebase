import { Component } from '@angular/core';
import{AngularFire} from 'angularfire2';
import {angularfire2} from 'AngularFire';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  
  constructor(af:AngularFire ) {
    console.log(af);
  }
}
