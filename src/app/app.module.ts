import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import{AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';

import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyDbNANhjkT5Tl5kKp9nz_TqkfgMwPksQIA",
    authDomain: "fir-6-28b9e.firebaseapp.com",
    databaseURL: "https://fir-6-28b9e.firebaseio.com",
    storageBucket: "fir-6-28b9e.appspot.com",
    messagingSenderId: "1079339711557"
}

const appRoutes:Routes = [
  {path:'users', component:UsersComponent},
  {path:'posts', component:PostsComponent},
  {path:'', component:UsersComponent},
  {path:'**', component:PageNotFoundComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    UserComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
